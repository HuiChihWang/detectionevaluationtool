from argparse import ArgumentParser
from data_utils import *
import os

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-p", "--path", default=".", type=str)
    args = parser.parse_args()

    data_folder = args.path
    file_list = [file for file in os.listdir(data_folder) if file.endswith('.json')]

    file_name = os.path.join(data_folder, file_list[0])
    load_test_data(file_name)



