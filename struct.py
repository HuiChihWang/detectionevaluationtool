from typing import Type


class Rectangle:
    def __init__(self, left_top=(0, 0), right_bottom=(0, 0)):
        self.__x0 = left_top[0]
        self.__y0 = left_top[1]
        self.__x1 = right_bottom[0]
        self.__y1 = right_bottom[1]

        assert self.__y1 >= self.__y0 and self.__x1 >= self.__x0

        self.__width = self.__x1 - self.__x0
        self.__height = self.__y1 - self.__y0

        self.__get_area()

    def __get_area(self):
        self.__area = self.__width * self.__height

    def get_area(self):
        return self.__area

    def get_points(self):
        return (self.__x0, self.__y0), (self.__x1, self.__y1)

    @staticmethod
    def __overlap_closure(closure1, closure2):
        overlap_left = max(closure1[0], closure2[0])
        overlap_right = min(closure1[1], closure2[1])

        if overlap_right > overlap_left:
            return [overlap_left, overlap_right]
        else:
            return []

    @staticmethod
    def intersect(rect1: 'Rectangle', rect2: 'Rectangle') -> 'Rectangle':
        rect1_tl, rect1_br = rect1.get_points()
        rect2_tl, rect2_br = rect2.get_points()

        x_overlap = Rectangle.__overlap_closure([rect1_tl[0], rect1_br[0]], [rect2_tl[0], rect2_br[0]])
        y_overlap = Rectangle.__overlap_closure([rect1_tl[1], rect1_br[1]], [rect2_tl[1], rect2_br[1]])

        if x_overlap and y_overlap:
            return Rectangle((x_overlap[0], y_overlap[0]), (x_overlap[1], y_overlap[1]))
        else:
            return Rectangle()

    @staticmethod
    def calculate_iou(rect1: 'Rectangle', rect2: 'Rectangle'):
        rect1_area = rect1.get_area()
        rect2_area = rect2.get_area()
        rect_overlap = Rectangle.intersect(rect1, rect2)
        overlap_area = rect_overlap.get_area()

        return overlap_area / float(rect1_area + rect2_area - overlap_area)
