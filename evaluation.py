from struct import Rectangle
from typing import List


class Evaluator:
    def __init__(self, threshold):
        self.threshold = threshold
        self.precision = 0
        self.recall = 0
        self.detect_result = {'TP': 0, 'FP': 0, 'FN': 0, 'TN': 0}

    def evaluate(self, gt_boxes: List[Rectangle], pred_boxes: List[Rectangle]):
        true_positive = 0
        for gt_box in gt_boxes:
            iou_list = []
            for pred_box in pred_boxes:
                iou = Rectangle.calculate_iou(gt_box, pred_box)
                iou_list.append(iou)

            if max(iou_list) > self.threshold:
                true_positive += 1

        false_positive = len(pred_boxes) - true_positive
        false_negative = len(gt_boxes) - true_positive

        self.detect_result['TP'] += true_positive
        self.detect_result['FP'] += false_positive
        self.detect_result['FN'] += false_negative

    @staticmethod
    def calculate_precision(true_positive, false_positive):
        return true_positive / float(true_positive + false_positive)

    @staticmethod
    def calculate_recall(true_positive, false_negative):
        return true_positive / float(true_positive + false_negative)
