from unittest import TestCase
from evaluation import Evaluator
from struct import Rectangle


class TestEvaluator(TestCase):
    def setUp(self) -> None:
        rect_1 = Rectangle((10, 20), (50, 60))
        rect_2 = Rectangle((0, 0), (20, 30))
        rect_3 = Rectangle((20, 20), (50, 70))
        rect_4 = Rectangle((10, 30), (50, 60))
        rect_5 = Rectangle((50, 60), (90, 90))

        self.test_pred_1 = [rect_2, rect_3]
        self.test_gt_1 = [rect_1]

        self.test_pred_2 = [rect_3, rect_4, rect_5]
        self.test_gt_2 = [rect_1, rect_2]

    def test_evaluate_single_meta_1(self):
        eval_tool = Evaluator(0.5)
        eval_tool.evaluate(gt_boxes=self.test_gt_1, pred_boxes=self.test_pred_1)

        self.assertEqual(eval_tool.detect_result['TP'], 1)
        self.assertEqual(eval_tool.detect_result['FP'], 1)
        self.assertEqual(eval_tool.detect_result['FN'], 0)

    def test_evaluate_single_meta_2(self):
        eval_tool = Evaluator(0.5)
        eval_tool.evaluate(gt_boxes=self.test_gt_2, pred_boxes=self.test_pred_2)

        self.assertEqual(eval_tool.detect_result['TP'], 1)
        self.assertEqual(eval_tool.detect_result['FP'], 2)
        self.assertEqual(eval_tool.detect_result['FN'], 1)

    def test_evaluate_multi_meta(self):
        eval_tool = Evaluator(0.5)
        eval_tool.evaluate(gt_boxes=self.test_gt_1, pred_boxes=self.test_pred_1)
        eval_tool.evaluate(gt_boxes=self.test_gt_2, pred_boxes=self.test_pred_2)

        self.assertEqual(eval_tool.detect_result['TP'], 2)
        self.assertEqual(eval_tool.detect_result['FP'], 3)
        self.assertEqual(eval_tool.detect_result['FN'], 1)
