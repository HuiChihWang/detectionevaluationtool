from unittest import TestCase
from struct import Rectangle


class TestRectangle(TestCase):
    def setUp(self) -> None:
        self.rect_empty = Rectangle()
        self.rect1 = Rectangle(left_top=(0, 0), right_bottom=(12, 10))
        self.rect2 = Rectangle(left_top=(5, 4), right_bottom=(15, 7))
        self.rect3 = Rectangle(left_top=(2, 3), right_bottom=(10, 7))
        self.rect4 = Rectangle(left_top=(-5, -7), right_bottom=(4, 3))
        self.rect5 = Rectangle(left_top=(-5, 5), right_bottom=(8, 10))

    def test_get_area(self):
        self.assertEqual(self.rect_empty.get_area(), 0)
        self.assertEqual(self.rect1.get_area(), 120)
        self.assertEqual(self.rect2.get_area(), 30)

    def test_get_points(self):
        rect_empty_point = self.rect_empty.get_points()
        rect1_point = self.rect1.get_points()
        rect3_point = self.rect3.get_points()

        self.assertEqual(rect_empty_point, ((0, 0), (0, 0)))
        self.assertEqual(rect1_point, ((0, 0), (12, 10)))
        self.assertEqual(rect3_point, ((2, 3), (10, 7)))

    def test_intersect(self):
        rect_intersect_12 = Rectangle.intersect(rect1=self.rect1, rect2=self.rect2)
        rect12_point = rect_intersect_12.get_points()
        self.assertEqual(rect12_point, ((5, 4), (12, 7)))

        rect_intersect_13 = Rectangle.intersect(rect1=self.rect1, rect2=self.rect3)
        rect13_point = rect_intersect_13.get_points()
        self.assertEqual(rect13_point, ((2, 3), (10, 7)))

        rect_intersect_24 = Rectangle.intersect(rect1=self.rect4, rect2=self.rect2)
        rect24_point = rect_intersect_24.get_points()
        self.assertEqual(rect24_point, ((0, 0), (0, 0)))

        rect_intersect_35 = Rectangle.intersect(rect1=self.rect3, rect2=self.rect5)
        rect35_point = rect_intersect_35.get_points()
        self.assertEqual(rect35_point, ((2, 5), (8, 7)))

    def test_calculate_iou(self):
        iou12 = Rectangle.calculate_iou(rect1=self.rect1, rect2=self.rect2)
        rect_intersect_12 = Rectangle.intersect(rect1=self.rect1, rect2=self.rect2)
        self.assertAlmostEqual(21. / (150. - 21.), iou12, delta=0.0001)
